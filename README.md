# README #

This is the primary utilities package for MMGE. It contains many different functions
that make doing common MMGE-related tasks in R easier. It is also a dependency 
of many of the other custom MMGE-related R packages.

### What is this repository for? ###

* Deploying shiny apps to the production server
* Publishing packages to the local CRAN-like Repository
* Saving reports to the HG-Data Drive
* Working with protocol-specific default values
* Restarting shiny apps on the development server
* Sending SSH commands between servers
* Reloading external packages during development

### How do I get set up? ###

This repository houses the source code for the mmge package. To use the mmge package
you should install it from MMGE's local CRAN-like repository.

### Who do I talk to? ###

Talk to Eric Bailey. He may know what he is doing. Or maybe he doesn't. Or maybe he does.