import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.utils import COMMASPACE, formatdate

def py_sendmail(send_from, send_to, subject, text_body = None, html_body = None, 
              inline = False, smtp = None, authenticate = False, 
              send = True, attach_files = None, debug = False):

  server = None

  if smtp['ssl'] == True:
    server = smtplib.SMTP_SSL('mail-relay.iu.edu', 465)
  else:
    server = smtplib.SMTP(smtp['host.name'], smtp['port'])

  if authenticate == True:
    server.login(smtp['user.name'], smtp['passwd'])

  msg = MIMEMultipart('alternative')
  msg['From']
  msg['To'] = send_to
  msg['Date'] = formatdate(localtime=True)
  msg['Subject'] = subject

  if attach_files is not None:

    for f in attach_files or []:
      with open(f, 'rb') as fil:
        part = MIMEApplication(
          fil.read(),
          Name=basename(f)
        )
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)
    
  if text_body is not None:
    msg.attach(MIMEText(text_body, 'plain'))
    
  if html_body is not None:
    msg.attach(MIMEText(html_body, 'html'))

  if send == True:
    x = server.sendmail(send_from, send_to, msg.as_string())

  server.quit()
  
  return x
