get_scripts <- function(root = "/media/HG-Data") {

  scripts <- list.files(root, recursive = TRUE, full.names = TRUE)
  scripts <- scripts[ grep("Scripts/.+\\.R", x = scripts)]

  return(scripts)

}