#'@export
unix2win <- function(file_path) {

  writeLines(text = readLines(file_path), con = file_path, sep = "\r\n")

}
