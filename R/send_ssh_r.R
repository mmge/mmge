#' Send R commands to remote servers
#'
#' @param server The name of the server to connect to
#' @param user The username to connect to on the remote server
#' @param \dots pasted together with spaces to create the R command to be run on the remote server
#'
#' Serves as a convenient wrapper for \code{\link{send_ssh}} for sending R commands
#' to a remote server. See the help for \code{\link{send_ssh}} for more details.
#'
#' @export
send_ssh_r <- function(..., server = "shiny2", user = "baileye", nse = TRUE, sudo = FALSE) {

  if(nse) {
    cmd <- deparse(nse(...))
  } else {
    cmd <- deparse(paste(list(...), collapse = " "))
  }

  print(cmd)

  if(sudo) {
    rcmd <- sprintf("sudo R -q -e %s", cmd)
  } else {
    rcmd <- sprintf("R -q -e %s", cmd)
  }

  send_ssh(rcmd, server = server, user = user)

}